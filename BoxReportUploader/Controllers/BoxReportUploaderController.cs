﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using OfficeOpenXml;
using System.Data.OleDb;
using System.Data;
using BoxReportUploader.Models;
using System.Globalization;

namespace BoxReportUploader.Controllers
{
    public class BoxReportUploaderController : Controller
    {
        // GET: BoxReportUploader
        public ActionResult Index(HttpPostedFileBase file)
        {
            string response = uploadreport1(file);
            ViewBag.response = response;
            return View();
        }
        public string uploadreport1(HttpPostedFileBase file)
        {
          
            if (Request.Files.Count > 0)
            {
                try
                {
                    object[,] obj = null;
                    int noOfCol = 0;
                    int noOfRow = 0;

                    if ((file != null) && (file.ContentLength > 0))
                    {
                        byte[] fileBytes = new byte[file.ContentLength];
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                        using (var package = new ExcelPackage(file.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            noOfCol = workSheet.Dimension.End.Column;
                            noOfRow = workSheet.Dimension.End.Row;
                            obj = new object[noOfRow, noOfCol];
                            ; obj = (object[,])workSheet.Cells.Value;
                            BoxModel m = new BoxModel();
                            List<BoxAttributes> li_at = m.getattributes("24");
                            insertintodatabase(obj,noOfRow,li_at);

                        }

                    }
                    }
             catch (Exception ex)
            {

            }
              
        }
            return "Report Uploaded";
        }
        string insertintodatabase(object[,]obj,int noofrow,List<BoxAttributes> li)
        {
            int rowindex = 1;
            for (int a= li.Count(); a<= noofrow;a++)
            {
                for (int b = 0; b < li.Count(); b++)
                {
                    DateTime datetime = DateTime.Parse((obj[rowindex, 5].ToString())); 
                    string s = "24" +","+ li[b].AttributeID + "," + rowindex.ToString() + "," + datetime + "," + obj[a, b].ToString();
                    Box.insert_attributes_boxreport("24", li[b].AttributeID, rowindex.ToString(), datetime.ToString(), obj[a, b].ToString());
                }
                rowindex++;
            }

           // int count = 1;
           // string[] split = obj[0, 9].ToString().Split(' ');
           // int month = DateTime.ParseExact(split[1], "MMM", CultureInfo.CurrentCulture).Month;
           // BoxModel x = new BoxModel();
           // int rowindex = 1;
           // int li_count = 0;
           // int check_count = 0;

           //for(int i=3;i<noofrow;i++)
           // {
           //     for (int j=0;j<129;j++)
           //     {
           //         string check = obj[i, j].ToString();
           //         if(check.Contains("Total"))
           //         {
           //             break;
           //         }
           //         else
           //         {
           //             if (j < 5)
           //             {
           //                 string datetime = "2019" + month +count;
           //                 Box.insert_attributes_boxreport("24", li[li_count].AttributeID, rowindex.ToString(), datetime, check);
           //                 li_count++;
           //                 if(j==4)
           //                 {
           //                     j = 9;
           //                 }

           //             }

           //             if(j>8)
           //             {
           //                 check_count++;
           //                 string datetime = li[li_count].AttributeName+month+count;
           //                 Box.insert_attributes_boxreport("24", li[li_count].AttributeID, rowindex.ToString(), datetime, obj[i,j].ToString());
           //                 li_count++;
           //                 if(li_count==9)
           //                 {
           //                     li_count = 5;
           //                 }
           //                 if(check_count==4)
           //                 {
           //                     count++;
           //                     check_count = 0;

           //                 }
           //             }
                        
           //         }
           //     }
           //     count = 1;
           //     li_count = 0;
           //     rowindex++;
           // }

            return "Report Uploaded";

        }
        public string uploadboxreport(HttpPostedFileBase file)
        {
            string filePath = "";
            if (file != null)
            {
                string path = Server.MapPath("~/Files/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                filePath = path + Path.GetFileName(file.FileName);
                string extension = Path.GetExtension(file.FileName);
                file.SaveAs(filePath);

                string conString = string.Empty;

                switch (extension)
                {
                    case ".xls": //Excel 97-03.
                        conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                        break;
                    case ".xlsx": //Excel 07 and above.
                        conString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                        break;
                }

                DataTable dt = new DataTable();
                conString = string.Format(conString, filePath);

                using (OleDbConnection connExcel = new OleDbConnection(conString))
                {
                    using (OleDbCommand cmdExcel = new OleDbCommand())
                    {
                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                        {
                            cmdExcel.Connection = connExcel;

                            //Get the name of First Sheet.
                            connExcel.Open();
                            DataTable dtExcelSchema;
                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                            connExcel.Close();

                            //Read Data from First Sheet.
                            connExcel.Open();
                            cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                            odaExcel.SelectCommand = cmdExcel;
                            odaExcel.Fill(dt);
                            connExcel.Close();
                        }
                    }
                }
               

            }
            return "";

        }
    }
}