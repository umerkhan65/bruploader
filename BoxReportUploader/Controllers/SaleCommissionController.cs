﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using BoxReportUploader.Models;
using System.IO;
using System.Globalization;

namespace BoxReportUploader.Controllers
{
    public class SaleCommissionController : Controller
    {
        public string sdatetype;
        public int srptid;
        public string ssdatetype;
        public int ssrptid;
        // GET: SaleCommission
        public ActionResult Index(string response)
        {
            if (response != "" && response != null)
            {
                ViewBag.response = response;
            }
            else
            {

                ViewBag.response = "";
            }
  
            ViewBag.ReportName = allreport();
            return View();
        }
        public List<Allattribute> allreport()
        {
            DataTable dt = SaleCommission.GetreportName();
            List<Allattribute> li_list = new List<Allattribute>();

            foreach (DataRow dr in dt.Rows)
            {
                Allattribute att = new Allattribute();
                att.Reportid = dr["ReportID"].ToString();
                att.ReportName = dr["ReportName"].ToString();
                li_list.Add(att);
            }
            return li_list;

        }

    

        [HttpPost]
        public ActionResult BtnuploadProcess(string reportid, HttpPostedFileBase file)
        {
            // Initializing response variable
            string response = "";
            // Setting response as per selected file
            response = uploadreport(file, Convert.ToInt32(reportid));
            // Setting response in viewbag
            ViewBag.response = response;
            ViewBag.ReportName = allreport();

            //if (response == "Already Commission Uplaod In Same Month")
            //{
            //    return PartialView("partialBtnuploadProcess");
            //}
            //else
            //{
            ViewBag.datetype = sdatetype;
            ViewBag.rptid = srptid;
            return View("Index");
            //}
        }
        [HttpPost]
        public ActionResult DeleteSamerecord(string rpt,string Dateed)
        {
            string del = SaleCommission.Delete_commissionDateLog(rpt,Dateed);
            string stri = SaleCommission.Delete_commissionDate(Convert.ToString(rpt));
            ViewBag.ReportName = allreport();
            string response = "Delete Commission && Re upload again";
            return RedirectToAction("Index", "SaleCommission", new { response = response });
        }
        public string uploadreport(HttpPostedFileBase file, int report_id)
        {
            // Setting report ID 
            int report_ID = report_id;
            // Initializing variable to return message
            string message = "";
            // Checking File Existence
            if (Request.Files.Count > 0)
            {
                try
                {
                    // Object for recording file data, counter for file's rows and columns
                    object[,] obj = null;
                    int noOfCol = 0;
                    int noOfRow = 0;
                    // Looking for data in file
                    if ((file != null) && (file.ContentLength > 0))
                    {
                        // Initializing and setting byte type variable to record file length
                        byte[] fileBytes = new byte[file.ContentLength];
                        // Reading file and Setting data 
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                        using (var package = new OfficeOpenXml.ExcelPackage(file.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            noOfCol = workSheet.Dimension.End.Column;
                            noOfRow = workSheet.Dimension.End.Row;
                            obj = new object[noOfRow, noOfCol];
                            obj = (object[,])workSheet.Cells.Value;
                            // Setting Commission Report List Attributes
                            SaleCommission Sal = new SaleCommission();
                            List<SaleCommissionAttributes> all_at = Sal.getattributes(report_id.ToString());

                            message = insertintodatabase_eom(obj, noOfRow, noOfCol, all_at, report_ID);
                          
                        }


                    }
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }

            }
            return message;
        }

     


        // Function for Inserting EOM File Records to database
        string insertintodatabase_eom(object[,] obj, int noofrow, int noofcol, List<SaleCommissionAttributes> li, int report_id)
        {

            try
            {


                //DataTable dtname = SaleCommission.getattributes(report_id.ToString());




                var dictionary = new Dictionary<int, string>();
                // Getting file headers
                for (int a = 0; a < 1; a++)
                {
                    for (int b = 0; b < noofcol; b++)
                    {
                        dictionary.Add(b, obj[a, b].ToString());
                    }
                }
                // validation

                DataTable d = SaleCommission.Getatt(report_id.ToString());
                int oo = 0;
                for (int dic = 0; dic < dictionary.Count; dic++)
                {
                    var attnna = dictionary[dic].ToLower();
                    DataRow[] row = d.Select("AttributeName='"+ attnna + "'");
                    if (row.Length > 0)
                    {
                        oo += 1;
                    }
                    else
                    {
                        continue;
                    }

                };

                if (oo != d.Rows.Count)
                {
                    return "Please Check (Space Issue & Name Issue & Missing columns)";
                }



                var payperiods = dictionary.Where(pair => pair.Value.ToLower() == ("Pay period").ToLower())
                                 .Select(pair => pair.Key)
                                 .FirstOrDefault();
                var ss = obj[1, payperiods];
                string payper = Convert.ToString(ss);
                string ReptimeTo = payper.Replace("-","").Replace("/", "").Replace(" ", "").Replace("  ", "").Replace(",", "");
                string dateed = SaleCommission.Getpayperiod((report_id).ToString(), ReptimeTo);
                    if (dateed != "")
                    {
                    sdatetype = "";
                    sdatetype = dateed;
                    srptid = report_id;
                    return "Re Upload Commission File";

                }
                    else
                    {
                    string stri = SaleCommission.Delete_commissionDate(Convert.ToString(report_id));
                  
                   

                    }
                for (int dic = 0; dic < dictionary.Count; dic++)
                {
                    var attributename = dictionary[dic].ToLower();
                    var attributeid = li.Where(s => s.AttributeName.ToLower() == attributename)
                        .Select(s => s.AttributeID).FirstOrDefault();
                    var CDataTypeID = li.Where(s => s.AttributeName.ToLower() == attributename)
                      .Select(s => s.CDataType_ID).FirstOrDefault();
                    
                    if (attributeid != null)
                    {

                        for (int a = 1; a < noofrow; a++)
                        {
                  
                            var Payperiod = dictionary.Where(pair => pair.Value.ToLower() == "pay period")
                               .Select(pair => pair.Key)
                               .FirstOrDefault();
                            var Payperiods = obj[a, Payperiod];
                        string ReptimeFrom = Convert.ToString(Payperiods);
                            if (ReptimeFrom == null)
                            {
                                return "Set Pay Period In Report";
                            }

                            
                            var key = dictionary.Where(pair => pair.Value.ToLower() == "emp id")
                                .Select(pair => pair.Key)
                                .FirstOrDefault();
                            var emp_id = obj[a, key];
                            string empId = emp_id.ToString();
                            string emp_ID = SaleCommission.getEmpID(empId);
                            string CDataType_ID = SaleCommission.GetCDatatype(CDataTypeID);
                            if (emp_ID == "")
                            {
                                emp_ID = "0";
                            }
                            else
                            {
                                emp_ID = emp_ID;
                            }
                            string datafield = "";
                            if (obj[a, dic] != null)
                            {
                                datafield = obj[a, dic].ToString();
                            }
                            else
                            {
                                datafield = "";
                            }
                            if (report_id != 6)
                            {
                                if (CDataType_ID == "2")
                                {
                                    SaleCommission.insert_attributes_commissionreport(emp_ID, report_id.ToString(), attributeid, "", datafield, "", ReptimeTo, ReptimeFrom);
                                }
                                else if (CDataType_ID == "3")
                                {
                                    SaleCommission.insert_attributes_commissionreport(emp_ID, report_id.ToString(), attributeid, "", "", datafield, ReptimeTo, ReptimeFrom);
                                }
                                else if (CDataType_ID == "1")
                                {
                                    SaleCommission.insert_attributes_commissionreport(emp_ID, report_id.ToString(), attributeid, datafield, "", "", ReptimeTo, ReptimeFrom);
                                }
                            }
                            else if (report_id == 6)
                            {

                               string rec1 =  SaleCommission.Insert_CommissionAttribute(emp_ID, report_id.ToString(),attributeid, datafield, ReptimeTo, CDataType_ID, ReptimeFrom);

                            }
                    
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                return "Uploaded data is not the correct format Rows:( "+ noofrow + " ) and  Columns:( "+ noofcol + " )";
            }


            return "Report Uploaded";

        }


    }
}