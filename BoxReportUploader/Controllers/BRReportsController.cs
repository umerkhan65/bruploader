﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using OfficeOpenXml;
using System.Data.OleDb;
using System.Data;
using BoxReportUploader.Models;
using System.Globalization;

namespace BRReports.Controllers
{
    public class BRReportsController : Controller
    {
        // GET: BoxReportUploader
        public ActionResult Index(HttpPostedFileBase file)
        {
            string response = "";
            response = uploadreport1(file);
            ViewBag.response = response;
            return View();
        }
        public string uploadreport1(HttpPostedFileBase file)
        {
            string message = "";
          
            if (Request.Files.Count > 0)
            {
                try
                {
                    object[,] obj = null;
                    int noOfCol = 0;
                    int noOfRow = 0;

                    if ((file != null) && (file.ContentLength > 0))
                    {
                        byte[] fileBytes = new byte[file.ContentLength];
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                        BoxModel m = new BoxModel();
                        List<BoxAttributes> li_at = m.getattributes("25");

                        using (var package = new ExcelPackage(file.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            noOfCol = workSheet.Dimension.End.Column;
                            noOfRow = workSheet.Dimension.End.Row;
                            obj = new object[noOfRow, noOfCol];
                            obj = (object[,])workSheet.Cells.Value;                                 
                            message= insertintodatabase(obj,noOfRow,li_at);
                        }
                    }
                    }
             catch (Exception ex)
            {
                    message = ex.ToString();
            }

            }
            return message;
        }
        string insertintodatabase(object[,]obj,int noofrow,List<BoxAttributes> li)
        {
            Box.DeleteOld_BoxReportData();
            int rowindex = 1;
            string ReportTime = DateTime.Now.ToString("yyyyMMddhhmm");

            try
            {
                for (int a = 1; a < noofrow; a++)
                {
                    for (int b = 3; b < li.Count(); b++)
                    {

                        if (b != 4)
                        {
                           // DateTime datetime = DateTime.Parse((obj[rowindex, 5].ToString()));
                            Box.insert_attributes_boxreport("25", li[b].AttributeID, rowindex.ToString(),ReportTime, obj[a, b].ToString());
                        }

                       
                    }
                    rowindex++;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }


            return "Report Uploaded";

        }
        public string uploadboxreport(HttpPostedFileBase file)
        {
            string filePath = "";
            if (file != null)
            {
                string path = Server.MapPath("~/Files/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                filePath = path + Path.GetFileName(file.FileName);
                string extension = Path.GetExtension(file.FileName);
                file.SaveAs(filePath);

                string conString = string.Empty;

                switch (extension)
                {
                    case ".xls": //Excel 97-03.
                        conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                        break;
                    case ".xlsx": //Excel 07 and above.
                        conString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES'";
                        break;
                }

                DataTable dt = new DataTable();
                conString = string.Format(conString, filePath);

                using (OleDbConnection connExcel = new OleDbConnection(conString))
                {
                    using (OleDbCommand cmdExcel = new OleDbCommand())
                    {
                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                        {
                            cmdExcel.Connection = connExcel;

                            //Get the name of First Sheet.
                            connExcel.Open();
                            DataTable dtExcelSchema;
                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                            connExcel.Close();

                            //Read Data from First Sheet.
                            connExcel.Open();
                            cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                            odaExcel.SelectCommand = cmdExcel;
                            odaExcel.Fill(dt);
                            connExcel.Close();
                        }
                    }
                }
               

            }
            return "";

        }
    }
}