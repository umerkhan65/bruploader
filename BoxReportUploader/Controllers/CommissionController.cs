﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BoxReportUploader.Models;
using OfficeOpenXml;
using System.Globalization;

namespace BoxReportUploader.Controllers
{
    public class CommissionController : Controller
    {
       
        // GET: Comission
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
      
        // Main Function invoked by Form Submission
        public ActionResult Index(HttpPostedFileBase file, int report_id)
        {
            // Initializing response variable
            string response = "";
            // Setting response as per selected file
            response = uploadreport(file, report_id);
            // Setting response in viewbag
            ViewBag.response = response;
            return View();

        }

        //public static string ToNullSafeString(this object obj)
        //{
        //    return (obj ?? string.Empty).ToString();
        //}
        // Upload report function
        public string uploadreport(HttpPostedFileBase file, int report_id)
        {
            // Setting report ID 
            int report_ID = report_id;
            // Initializing variable to return message
            string message = "";
            // Checking File Existence
            if (Request.Files.Count > 0)
            {
                try
                {
                    // Object for recording file data, counter for file's rows and columns
                    object[,] obj = null;
                    int noOfCol = 0;
                    int noOfRow = 0;
                    // Looking for data in file
                    if ((file != null) && (file.ContentLength > 0))
                    {
                        // Initializing and setting byte type variable to record file length
                        byte[] fileBytes = new byte[file.ContentLength];
                        // Reading file and Setting data 
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                        using (var package = new ExcelPackage(file.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            noOfCol = workSheet.Dimension.End.Column;
                            noOfRow = workSheet.Dimension.End.Row;
                            obj = new object[noOfRow, noOfCol];
                            obj = (object[,])workSheet.Cells.Value;
                            // Setting Commission Report List Attributes
                             CommissionModel cm = new CommissionModel();
                            List<CommissionAttributes> li_at = cm.getattributes(report_ID.ToString());
                            // Checking for Selected report File
                            if (report_id == 2)
                            {
                                // Inserting Records to database
                                message = insertintodatabase_eom(obj, noOfRow, noOfCol, li_at, report_ID);
                            }
                            else
                            {
                                // Inserting Records to database
                                message = insertintodatabase_rsm(obj, noOfRow, noOfCol, li_at, report_ID);

                            }

                        }


                    }
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }

            }
            return message;
        }

        

        // Function for Inserting EOM File Records to database
        string insertintodatabase_eom(object[,] obj, int noofrow,int noofcol, List<CommissionAttributes> li, int report_id)
        {

            try
            {
                var dictionary = new Dictionary<int, string>();
                // Getting file headers
                for (int a = 0;a<1;a++)
                {
                    for(int b = 0;b<noofcol;b++)
                    {
                        dictionary.Add(b, obj[a, b].ToString());
                    }
                }
                
                for (int dic = 0; dic < dictionary.Count; dic++)
                {
                    var attributename = dictionary[dic].ToLower();
                    var attributeid = li.Where(s => s.AttributeName.ToLower() == attributename)
                        .Select(s => s.AttributeID).FirstOrDefault();
                    
                    if (attributeid != null)
                    {

                        for (int a = 1; a < noofrow; a++)
                        {
                            var key = dictionary.Where(pair => pair.Value.ToLower() == "emp id")
                                .Select(pair => pair.Key)
                                .FirstOrDefault();
                            var emp_id = obj[a, key];
                            string empId = emp_id.ToString();
                            string emp_ID = CommissionModel.getEmpID(empId);
                            if(emp_ID == "")
                            {
                                emp_ID = "0";
                            }
                            else
                            {
                                emp_ID = emp_ID;
                            }
                            string datafield = "";
                            if(obj[a,dic] != null)
                            {
                                datafield = obj[a, dic].ToString();
                            }
                            else
                            {
                                datafield = "";
                            }
                            if (attributeid == "31" || attributeid == "33" || attributeid == "39" || attributeid == "38")
                            {
                                Commission.insert_attributes_commissionreport(emp_ID, report_id.ToString(), attributeid, "", datafield, "");
                            }
                            else if (attributeid == "32" || attributeid == "34" || attributeid == "40" || attributeid == "42" || attributeid == "43" || attributeid == "49")
                            {
                                Commission.insert_attributes_commissionreport(emp_ID, report_id.ToString(), attributeid, "", "", datafield);
                            }
                            else
                            {
                                Commission.insert_attributes_commissionreport(emp_ID, report_id.ToString(), attributeid, datafield, "", "");
                            }
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }


            return "Report Uploaded";

        }
        
        // Function For Inserting RSM File Records To Database
        string insertintodatabase_rsm(object[,] obj, int noofrow, int noofcol, List<CommissionAttributes> li, int report_id)
        {
            try
            {

                var dictionary = new Dictionary<int, string>();
                // Getting file headers
                for (int a = 0; a < 1; a++)
                {
                    for (int b = 0; b < noofcol; b++)
                    {

                        dictionary.Add(b, obj[a, b].ToString());

                    }
                }

                for (int dic = 0; dic < dictionary.Count; dic++)
                {
                    var attributename = dictionary[dic].ToLower();
                    var attributeid = li.Where(s => s.AttributeName.ToLower() == attributename)
                        .Select(s => s.AttributeID).FirstOrDefault();


                    if (attributeid != null)
                    {

                        for (int a = 1; a < noofrow; a++)
                        {
                            var key = dictionary.Where(pair => pair.Value.ToLower() == "emp id")
                                .Select(pair => pair.Key).FirstOrDefault();
                            var emp_id = obj[a, key];
                            string empId = emp_id.ToString();
                            string emp_ID = CommissionModel.getEmpID(empId);
                            if (emp_ID == "")
                            {
                                emp_ID = "0";
                            }
                            else
                            {
                                emp_ID = emp_ID;
                            }
                            string datafield = "";
                            if (obj[a, dic] != null)
                            {
                                datafield = obj[a, dic].ToString();
                                if(attributeid == "7")
                                {
                                    DateTime dt = DateTime.FromOADate(Convert.ToDouble(datafield));
                                    string date = dt.ToString("d/M/yyyy");
                                    datafield = date;
                                }
                               
                               
                                else
                                {
                                    datafield = datafield;
                                }
                            }
                            else
                            {
                                datafield = "";
                            }
                            if (attributeid == "13" || attributeid == "17" || attributeid == "18" || attributeid == "19" || attributeid == "21")
                            {
                                Commission.insert_attributes_commissionreport(emp_ID, report_id.ToString(), attributeid, "", datafield, "");
                            }
                            else if (attributeid == "14" || attributeid == "16" || attributeid == "20" || attributeid == "47" || attributeid =="48")
                            {
                                Commission.insert_attributes_commissionreport(emp_ID, report_id.ToString(), attributeid, "", "", datafield);
                            }
                            else
                            {
                                Commission.insert_attributes_commissionreport(emp_ID, report_id.ToString(), attributeid, datafield, "", "");
                            }
                        }
                    }

                }

                
            }
            catch (Exception ex)
            {
                ex.ToString();
            }


            return "Report Uploaded";

        }

    }
}