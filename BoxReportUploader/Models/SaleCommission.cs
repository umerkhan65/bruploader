﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BoxReportUploader.Models
{
    public class SaleCommission
    {
        static string response = "";

        public static DataTable GetreportName()
        {
            DAL obj = new DAL();
            SPParameters sp = new SPParameters();
            obj.ProcName = "GetCommissionReportsName";
            DataTable dt =   obj.Getdata(sp);
            return dt;

        }
        public static DataTable Getatt(string reportid)
        {
            DAL obj = new DAL();
            SPParameters sp = new SPParameters();
            obj.ProcName = "GetCommissionsAttributesNew";
            sp.SetParam("reportid", SqlDbType.Int, reportid);
            DataTable dt = obj.Getdata(sp);

            return dt;

        }
        public List<SaleCommissionAttributes> getattributes(string reportid)
        {
            DAL obj = new DAL();
            List<SaleCommissionAttributes> li_at = new List<SaleCommissionAttributes>();
            obj.ProcName = "GetCommissionsAttributesNew";
            SPParameters sp = new SPParameters();
            sp.SetParam("reportid", SqlDbType.Int, reportid);
            DataTable dt = obj.Getdata(sp);
            foreach (DataRow dr in dt.Rows)
            {
                SaleCommissionAttributes obj_x = new SaleCommissionAttributes();
                obj_x.AttributeID = dr["AttributeId"].ToString();
                obj_x.AttributeName = dr["AttributeName"].ToString();
                obj_x.CDataType_ID = dr["CDataType_ID"].ToString();
                li_at.Add(obj_x);
            }
            return li_at;
        }

        public static string getEmpID(string emp_id)
        {
            DAL obj = new DAL();
            string empID = "";
            obj.ProcName = "GetUserRecIDByEmpID";
            SPParameters sp = new SPParameters();
            sp.SetParam("empid", SqlDbType.NVarChar, emp_id);
            DataTable dt = obj.Getdata(sp);
            if (dt.Rows.Count > 0)
            {
                empID = dt.Rows[0]["ID"].ToString();
            }
            return empID;
        }

        public static string GetCDatatype(string CDatatype_ID)
        {
            DAL obj = new DAL();
            string CDataID = "";
            obj.ProcName = "GetCommissionDataType";
            SPParameters sp = new SPParameters();
            sp.SetParam("CDataTypeID", SqlDbType.NVarChar, CDatatype_ID);
            DataTable dt = obj.Getdata(sp);
            if (dt.Rows.Count > 0)
            {
                CDataID = dt.Rows[0]["ID"].ToString();
            }
            return CDataID;
        }

        public static string Getpayperiod(string ReportID,string Reptime)
        {
            DAL obj = new DAL();
            string CDataID = "";
            obj.ProcName = "GetreportDataCheck";
            SPParameters sp = new SPParameters();
            sp.SetParam("ReportID", SqlDbType.Int, ReportID);
            sp.SetParam("Reptime", SqlDbType.NVarChar, Reptime);
            DataTable dt = obj.Getdata(sp);
            if (dt.Rows.Count > 0)
            {
                CDataID = dt.Rows[0][0].ToString();
            }
            return CDataID;
        }


        public static string insert_attributes_commissionreport(string userid, string reportid, string attributeid, string data, string achieved, string commission,string RepTime,string RepTimeFrom)
        {
            DAL obj = new DAL();
            obj.ProcName = "InsertCommissionNew";
            SPParameters sp = new SPParameters();
            sp.SetParam("UserId", SqlDbType.Int, userid);
            sp.SetParam("AttributeId", SqlDbType.Int, attributeid);
            sp.SetParam("reportid", SqlDbType.Int, reportid);
            sp.SetParam("data", SqlDbType.NVarChar, data);
            sp.SetParam("achieved", SqlDbType.NVarChar, achieved);
            sp.SetParam("commission", SqlDbType.NVarChar, commission);
            sp.SetParam("RepTime", SqlDbType.NVarChar, RepTime);
            sp.SetParam("RepTimeFrom", SqlDbType.NVarChar, RepTimeFrom);
            response = obj.AddData(sp);
            return response;

        }
        public static string Insert_CommissionAttribute(string userid, string reportid, string attributeid, string data, string RepTime,string Attributetypeid,string RepTimeFrom)
        {
            DAL obj = new DAL();
            obj.ProcName = "InsertCommissionNewDynamic";
            SPParameters sp = new SPParameters();
            sp.SetParam("UserId", SqlDbType.Int, userid);
            sp.SetParam("AttributeId", SqlDbType.Int, attributeid);
            sp.SetParam("reportid", SqlDbType.Int, reportid);
            sp.SetParam("data", SqlDbType.NVarChar, data);
            sp.SetParam("RepTime", SqlDbType.NVarChar, RepTime); 
            sp.SetParam("AttributeTypeID", SqlDbType.Int, Attributetypeid);
            sp.SetParam("RepTimeFrom", SqlDbType.NVarChar, RepTimeFrom);
            response = obj.AddData(sp);
            return response;

        }
        public static string Delete_commissionDate(string reportid)
        {
            DAL obj = new DAL();
            obj.ProcName = "DeleteCommissionData";
            SPParameters sp = new SPParameters();
            sp.SetParam("reportid", SqlDbType.Int, reportid);
            response = obj.AddData(sp);
            return response;

        }
        public static string Delete_commissionDateLog(string reportid, string Reptime)
        {
            DAL obj = new DAL();
            obj.ProcName = "DeleteCommissionDataLog";
            SPParameters sp = new SPParameters();
            sp.SetParam("reportid", SqlDbType.Int, reportid);
            sp.SetParam("Reptime", SqlDbType.NVarChar, Reptime);
            response = obj.AddData(sp);
            return response;

        }
    }
    public class Allattribute
    {
        public string Reportid { get; set; }
        public string ReportName { get; set; }

    }
    public class SaleCommissionAttributes
    {
        public string AttributeID { get; set; }
        public string AttributeName { get; set; }
        public string CDataType_ID { get; set; }
    }

   


}