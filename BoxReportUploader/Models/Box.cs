﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BoxReportUploader.Models
{
    public class Box
    {
       static string response = "";
        
        public static string insert_attributes_boxreport(string reportid,string attributeid,string rowindex,string datetime,string data)
        {
            DAL obj = new DAL();
            obj.ProcName = "InsertBoxAttributes";
            SPParameters sp = new SPParameters();
            sp.SetParam("attributeid", SqlDbType.Int, attributeid);
            sp.SetParam("reportid", SqlDbType.Int, reportid);
            sp.SetParam("repdata", SqlDbType.NVarChar, data);
            sp.SetParam("reptime",SqlDbType.NVarChar, datetime);
            sp.SetParam("rowindex", SqlDbType.Int, rowindex);
            
            response=obj.AddData(sp);

            return response;

        }

        public static string DeleteOld_BoxReportData()
        {
            DAL obj = new DAL();
            obj.ProcName = "DeleteBoxReportData";
            SPParameters sp = new SPParameters();
            sp.SetParam("Confirm", SqlDbType.Int, "101");
            response = obj.ExecuteSP(sp);
            return response;
        }

    }
}