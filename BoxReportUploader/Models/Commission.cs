﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BoxReportUploader.Models
{
    public class Commission
    {
        // Initializing variable to return Message 
        static string response = "";
        // Function for setting Attributes
        public static string insert_attributes_commissionreport(string userid, string reportid, string attributeid, string data, string achieved, string commission)
        {
            DAL obj = new DAL();
            obj.ProcName = "InsertCommission";
            SPParameters sp = new SPParameters();
            sp.SetParam("UserId", SqlDbType.Int,userid);
            sp.SetParam("AttributeId", SqlDbType.Int, attributeid);
            sp.SetParam("reportid", SqlDbType.Int, reportid);
            sp.SetParam("data", SqlDbType.NVarChar, data);
            sp.SetParam("achieved", SqlDbType.NVarChar, achieved);
            sp.SetParam("commission", SqlDbType.NVarChar, commission);
            response = obj.AddData(sp);
            return response;

        }
    }
}