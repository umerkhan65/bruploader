﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BoxReportUploader.Models
{
    public class CommissionModel
    {
        // Function for Listing Attributes as per file
        public List<CommissionAttributes> getattributes(string reportid)
        {
            DAL obj = new DAL();
            List<CommissionAttributes> li_at = new List<CommissionAttributes>();
            obj.ProcName = "GetCommissionsAttributes";
            SPParameters sp = new SPParameters();
            sp.SetParam("reportid", SqlDbType.Int, reportid);
            DataTable dt = obj.Getdata(sp);
            foreach(DataRow dr in dt.Rows)
            {
                CommissionAttributes obj_x = new CommissionAttributes();
                obj_x.AttributeID = dr["AttributeId"].ToString();
                obj_x.AttributeName = dr["AttributeName"].ToString();
                li_at.Add(obj_x);    
            }
            return li_at;
        }

        public static string getEmpID(string emp_id)
        {
            DAL obj = new DAL();
            string empID = "";
            obj.ProcName = "GetUserRecIDByEmpID";
            SPParameters sp = new SPParameters();
            sp.SetParam("empid", SqlDbType.NVarChar, emp_id);
            DataTable dt = obj.Getdata(sp);
            if(dt.Rows.Count > 0)
            {
                empID = dt.Rows[0]["ID"].ToString();
            }
            return empID;
        }
    }
    public class CommissionAttributes
    {
        public string AttributeID { get; set; }
        public string AttributeName { get; set; }
    }
}