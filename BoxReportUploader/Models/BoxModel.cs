﻿
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BoxReportUploader.Models
{
    public  class BoxModel
    {
        public string Market { get; set; }
        public string Location { get; set; }
        public string SD { get; set; }
        public string TM { get; set; }
        public string UID { get; set; }
      
        public  List<BoxAttributes> getattributes(string reportid)
        {
            DAL obj = new DAL();
            List<BoxAttributes> li_at = new List<BoxAttributes>();
           
            obj.ProcName = "GetReportAttributesByReportID";
            SPParameters sp = new SPParameters();
            sp.SetParam("reportid", SqlDbType.Int, reportid);
            DataTable dt = obj.Getdata(sp);
            foreach(DataRow dr in dt.Rows)
            {
                BoxAttributes obj_x = new BoxAttributes();
                obj_x.AttributeID = dr["Attribute_Id"].ToString();
                obj_x.AttributeName = dr["Attribute_Name"].ToString();

                if (dr["Isactive"].ToString() == "1") { li_at.Add(obj_x); }
            }
            return li_at;
        }
    }
    public class BoxAttributes
    {
        public string AttributeID { get; set; }
        public string AttributeName { get; set; }
    }

    }
